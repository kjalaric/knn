'''
knn_test.py

Test script for knn.py
Requires data_generator.py (can be found in the "data-test" project)

kjalaric@gitlab

'''

import matplotlib.pyplot as plt
import numpy as np

import sys
sys.path.append('..')
import knn
import data_generator as dg

# generate reference data
cluster1, cluster2 = dg.two_clusters(std_dev=10, separate_lists=True)
cluster1x, cluster1y = dg.get_x_and_y(cluster1)
cluster2x, cluster2y = dg.get_x_and_y(cluster2)

cluster3, cluster4 = dg.two_clusters(std_dev=10, separate_lists=True)
cluster3x, cluster3y = dg.get_x_and_y(cluster3)
cluster4x, cluster4y = dg.get_x_and_y(cluster4)

# generate random point
x_point = np.random.uniform(0, 100)
y_point = np.random.uniform(0, 100)
test_data = (x_point, y_point)

# run classifier
classifier = knn.KNNClassifier(cluster1, cluster2, cluster3, cluster4)
category = classifier.classify(test_data)

plt.scatter(cluster1x, cluster1y, marker="s")
plt.scatter(cluster2x, cluster2y, marker="o")
plt.scatter(cluster3x, cluster3y, marker="^")
plt.scatter(cluster4x, cluster4y, marker="+")

if (category == 0):
    plt.scatter(x_point, y_point, marker="s", s=200)
elif (category == 1):
    plt.scatter(x_point, y_point, marker="o", s=200)
elif (category == 2):
    plt.scatter(x_point, y_point, marker="^", s=200)
elif (category == 3):
    plt.scatter(x_point, y_point, marker="+", s=200)
plt.show()

