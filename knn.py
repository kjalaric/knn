'''
knn.py

Implementation of k-nearest neigbours regression algorithm in python.

example use:
$ knn = KNNClassifier(data_for_category_0, data_for_category_1, data_for_category_2)
$ category = knn.classify(predictor_data_point)
where "data_for_category_*" is a list of tuples (each of size x), and "predictor_data_point" is a tuple of size x.
the return variable ("category" in this case) is the category assigned to "predictor_data_point", corresponding to the
    order in which the lists were provided to KNNClassifier().
KNNClassifier can take an arbitrary number of lists, and they don't necessarily need to be the same size.

kjalaric@gitlab

'''

def tuple_norm(t1, t2):
    if (len(t1) != len(t2)):
        raise Exception("data points are of different shape")
    sum_of_square_differences = 0
    for i in range(len(t1)):
        sum_of_square_differences += (t1[i] - t2[i])**2.0
    return sum_of_square_differences**0.5
        

class KNNClassifier(object):
    '''
    takes any number of tuple lists as an input argument, and uses these lists when
    comparing datapoints with KNNClassifier.classify()
    '''
    def __init__(self, *input_data_vectors):
        self.data = list()
        for idv in input_data_vectors:
            self.data.append(idv)
        
    def classify(self, data_point, verbose=False):
        scores = list()
        for data in self.data:
            score = 0
            for point in data:
                score += tuple_norm(data_point, point)
            score /= len(data)
            scores.append(score)
        
        lowest_score_index = 0
        for i in range(1, len(scores)):
            if (scores[i] < scores[lowest_score_index]):  # does not handle the case of multiple clusters having the exact same distance score
                lowest_score_index = i
        
        if (verbose):
            print (scores)
            print ("Lowest score is for category {}.".format(lowest_score_index))
        
        return lowest_score_index